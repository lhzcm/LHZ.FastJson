﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LHZ.FastJson.Enum
{
    public enum ObjectType
    {
        Boolean,
        Int,
        Long,
        Float,
        Double,
        Decimal,
        DateTime,
        Enum,
        String,
        Dictionary,
        Array,
        List,
        Enumerable,
        Object
    }
}
