﻿using LHZ.FastJson.JsonClass;
using System;
using System.Collections.Generic;
using System.Text;

namespace LHZ.FastJson.Interface
{
    public interface IJsonReader
    {
        JsonObject JsonRead();
    }
}
